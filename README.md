# Présentation du projet jeu 0.7.42

*ce projet a pour but d'approfondir nos connaissances en javascript dans un contexte inhabituel, mais de pouvoir les réutiliser plus tard dans  des projets plus concrets.*


---

## Technologies utilisées 



* HTML
* CSS
* JavaScript (Variables, Fonctions, Conditions, Evenements, POO)

---


## Intérêt 


* mise en pratique des connaissances
* utiliser le JavaScript dans un autre contexte
* pour le portfolio

---

## quelque graphisme present dans le jeu

**Voila:**

<img src="image/heroKick.png">
<img src="image/heroFist.png">
<img src="image/necromancer.png">

---

## Versions à venir

* mise en place du dodge et du coup critique.
* mise en place des niveaux du hero mise en place du gain d'argent.
* amelioration de la rotation des combats en foret.
* mettre en place le village.
* amelioration du systeme d'objet.
**amelioration du background de l'histoire**

---

## Modifications de la 0.7.42

cette mise à jour majeure a ajouté la fonctionnalité de pouvoir affronter le **vrai** boss de fin.

---

## Version test

Le jeu n'est pas encore dans une version vraiment jouable donc des améliorations sont à venir.

---

## Licence  

Mon projet utilise la licence Beerware, pour plus d'**INFORMATION :**  [Wikipedia](https://fr.wikipedia.org/wiki/Beerware)


  
  **"LA LICENCE BEER-WARE"**

  <img src="image/BeerWare.jpg">
 
 *Du moment que vous utilisez mon code avec cette licence, vous pouvez en faire ce que vous voulez.*
 
 **Néanmoins si mon projet vous plait, et qu'on se rencontre en personne vous pouvez m'offrir une bière.** 
 
 (apparement ça marche comme ça)
  