// systeme de combat

// CONSOLE DE COMBAT starts here!

// PUNCH 


punchcommande.addEventListener('click', function (event) {

    punch(hero, bigMonster);
    swapconsole(consoleAttack, commande);
    if (endFight(hero, bigMonster, resultFight) === 1) {
        aaa
    }
    if (bigMonster.health > 0) {
        opponentTurn(hero, bigMonster);
        verifDialogue = 1;
        dialoguebox(verifDialogue);

    }
});


function punch(hero, opponent) {
    opponent.health = opponent.health - hero.havoc;
    let opponentBar = document.querySelector(`.opponentBar span`);
    let pourcentageVie = Math.floor((opponent.health / opponent.maxHealth) * 100);
    opponentBar.style.width = `${pourcentageVie}%`;
    if (opponent.health <= 0) {
        opponentBar.style.width = `0%`;
    }
}


// KICK 


kickcommande.addEventListener('click', function (event) {

    kick(hero, bigMonster);
    swapconsole(consoleAttack, commande);
    if (endFight(hero, bigMonster, resultFight) === 1) {
        aaa
    }
    if (bigMonster.health > 0) {
        opponentTurn(hero, bigMonster);
        verifDialogue = 1;
        dialoguebox(verifDialogue);

    }
});

function kick(hero, opponent) {
    opponent.health = opponent.health - (hero.havoc - 5);
    opponent.havoc = opponent.havoc - 2;
    let opponentBar = document.querySelector(`.opponentBar span`);
    let pourcentageVie = Math.floor((opponent.health / opponent.maxHealth) * 100);
    opponentBar.style.width = `${pourcentageVie}%`;
    if (opponent.health <= 0) {
        opponentBar.style.width = `0%`;
    }
}



// OVERTHROW


overthrowcommande.addEventListener('click', function (event) {

    overthrow(hero, bigMonster);
    swapconsole(consoleAttack, commande);
    verifDialogue = 1;
    dialoguebox(verifDialogue);
    document.querySelector(".text-adjust p").innerHTML = `${bigMonster.name}: <br> health: ???? <br> havoc: ???? <br> meal: you <br> friend: ????  <br> fear: nothing `;
    if (endFight(hero, bigMonster, resultFight) === 1) {
        aaa

    }
});

function overthrow(hero, opponent) {
    opponent.health = opponent.health - 15;
    hero.dodge = hero.dodge + 5;
    let opponentBar = document.querySelector(`.opponentBar span`);
    let pourcentageVie = Math.floor((opponent.health / opponent.maxHealth) * 100);
    opponentBar.style.width = `${pourcentageVie}%`;
    if (opponent.health <= 0) {
        opponentBar.style.width = `0%`;
    }
}


//  WEAPON SHOT


weaponshotcommande.addEventListener('click', function (event) {

    weapon(hero, bigMonster);
    swapconsole(consoleAttack, commande);
    if (endFight(hero, bigMonster, resultFight) === 1) {
        aaa
    }
    if (bigMonster.health > 0) {
        opponentTurn(hero, bigMonster);
        verifDialogue = 1;
        dialoguebox(verifDialogue);

    }
});


function weapon(hero, opponent) {
    opponent.health = opponent.health - (hero.havoc + hero.weapon);
    let opponentBar = document.querySelector(`.opponentBar span`);
    let pourcentageVie = Math.floor((opponent.health / opponent.maxHealth) * 100);
    opponentBar.style.width = `${pourcentageVie}%`;
    if (opponent.health <= 0) {
        opponentBar.style.width = `0%`;
    }
}


// CONSOLE DE COMBAT ends here!


// CONSOLE DE SPECIAL

// SPECIAL


mantracommande.addEventListener('click', function (event) {

    mantra(hero, bigMonster);
    swapconsole(consoleSpecial, commande);
    if (endFight(hero, bigMonster, resultFight) === 1) {
        aaa
    }
    if (bigMonster.health > 0) {
        opponentTurn(hero, bigMonster);
        verifDialogue = 1;
        dialoguebox(verifDialogue);

    }
});


function mantra(hero, opponent) {
    hero.health = hero.health + 25;
    hero.havoc = hero.havoc + 15;
    hero.criticalStrike = hero.criticalStrike + 25;
}




// CONSOLE OBJET



objectcommande.addEventListener('click', function (event) {
    let empty = 0;
    if (empty === 0) {
        object1(hero, bigMonster, empty);
        empty = 1;
        swapconsole(consoleObject, commande);
        hero.inventory[0] = 'empty';
        if (endFight(hero, bigMonster, resultFight) === 1) {

        }
        if (bigMonster.health > 0 && bigMonster.tame < 2) {
            opponentTurn(hero, bigMonster);
            verifDialogue = 1;
            dialoguebox(verifDialogue);
        }
    } else if (empty === 1) {
        verifDialogue = 1;
        dialoguebox(verifDialogue);
        document.querySelector(".consoleObject .object1 .console-tag").innerHTML = `${hero.inventory[0]}`;
    }

});


function object1(hero, opponent, empty) {
    opponent.tame = opponent.tame + 1;
    empty = 1;
}






object2commande.addEventListener('click', function (event) {

    object2(hero, bigMonster);
    swapconsole(consoleObject, commande);
    hero.inventory[1] = 'empty';
    document.querySelector(".consoleObject .object1 .console-tag").innerHTML = `${hero.inventory[1]}`;
    if (endFight(hero, bigMonster, resultFight) === 1) {
        aaa
    }
    if (bigMonster.health > 0 && bigMonster.tame < 2) {
        opponentTurn(hero, bigMonster);
        verifDialogue = 1;
        dialoguebox(verifDialogue);

    }
});


function object2(hero, opponent) {
    opponent.tame = opponent.tame + 1;
}




// CONSOLE DE DEFENSE 

// guard


guardcommande.addEventListener('click', function (event) {

    guard(hero, bigMonster);
    swapconsole(consoleDefense, commande);
    if (endFight(hero, bigMonster, resultFight) === 1) {
        aaa
    }
    if (bigMonster.health > 0) {
        opponentTurn(hero, bigMonster);
        verifDialogue = 1;
        dialoguebox(verifDialogue);

    }
});

function guard(hero, opponent, tame) {
    hero.health = hero.health + (15);
    hero.havoc = hero.havoc + 5;
    hero.position = 'wolf';
    opponent.tame = opponent.tame + 1;
}




// riposte


ripostecommande.addEventListener('click', function (event) {

    riposte(hero, bigMonster);
    swapconsole(consoleDefense, commande);
    if (endFight(hero, bigMonster, resultFight) === 1) {
        aaa
    }
    if (bigMonster.health > 0) {
        document.querySelector(".text-adjust p").innerHTML = `${bigMonster.name}: <br> health: ,?}=!§) <br> havoc: !§)[?Ç{ <br> meal: you <br> friend: <,?!§)}  <br> fear: nothing `;
        verifDialogue = 1;
        dialoguebox(verifDialogue);

    }
});


function riposte(hero, opponent) {
    opponent.health = opponent.health - opponent.havoc;
    hero.dodge = hero.dodge + 5;
    let opponentBar = document.querySelector(`.opponentBar span`);
    let pourcentageVie = Math.floor((opponent.health / opponent.maxHealth) * 100);
    opponentBar.style.width = `${pourcentageVie}%`;
    if (opponent.health <= 0) {
        opponentBar.style.width = `0%`;
    }
}



// crushcounter


crushcountercommande.addEventListener('click', function (event) {

    crushcounter(hero, bigMonster);
    swapconsole(consoleDefense, commande);
    if (endFight(hero, bigMonster, resultFight) === 1) {
        aaa
    }
    if (bigMonster.health > 0) {
        opponentTurn(hero, bigMonster);
        verifDialogue = 1;
        dialoguebox(verifDialogue);

    }
});

function crushcounter(hero, opponent) {
    opponent.health = opponent.health - (40 % hero.maxHealth);
    let opponentBar = document.querySelector(`.opponentBar span`);
    let pourcentageVie = Math.floor((opponent.health / opponent.maxHealth) * 100);
    opponentBar.style.width = `${pourcentageVie}%`;
    if (opponent.health <= 0) {
        opponentBar.style.width = `0%`;
    }
}



// leave


leavecommande.addEventListener('click', function (event) {

    leave(hero, bigMonster);
    swapconsole(consoleDefense, commande);
    document.querySelector(".text-adjust p").innerHTML = 'you leave';
    verifDialogue = 1;
    dialoguebox(verifDialogue);
    fuite = 1;

});

function leave(hero, opponent) {
    hero.dodge = hero.dodge + 5;
}






// EVENT DE FIGHT 
function opponentTurn(hero, opponent) {

    hero.health = hero.health - opponent.havoc;
    document.querySelector(".text-adjust p").innerHTML = `${bigMonster.name}: <br> health: ???? <br> havoc: ???? <br> meal: you <br> friend: ????  <br> fear: nothing `;
    // document.querySelector(".text-adjust p").innerHTML = `${hero.name}: <br> health:${hero.health} <br> havoc:${hero.havoc}`;
    // verifDialogue = 1;
    // dialoguebox(verifDialogue);
    let heroBar = document.querySelector(`.heroBar span`);
    let pourcentageVie = Math.floor((hero.health / hero.maxHealth) * 100);
    if (pourcentageVie > 100) {
        heroBar.style.backgroundColor = 'rgb(218, 129, 14)';
        heroBar.style.width = `110%`;
    } else {
        heroBar.style.backgroundColor = 'rgb(187, 21, 21)';
        heroBar.style.width = `${pourcentageVie}%`;
    }
    if (hero.health <= 0) {
        heroBar.style.width = `0%`;
        verifDialogue = 1;
        dialoguebox(verifDialogue);
        document.querySelector(".text-adjust p").innerHTML = `you are in bad shape you start to lose consciousness little by little .. `;
    }
}


function endFight(hero, opponent, result) {
    if (opponent.health <= 0) {
        result = 1;
        verifDialogue = 1;
        dialoguebox(verifDialogue);
        document.querySelector(".text-adjust p").innerHTML = `${opponent.name} is dead`;

    }
    if (hero.health <= 0) {
        result = 1;
        verifDialogue = 1;
        dialoguebox(verifDialogue);
        document.querySelector(".text-adjust p").innerHTML = `you are in bad shape you start to lose consciousness little by little .. `;
        loose = 1;

    }
    return (result);
}