// CHANGEMENT DE CONSOLE 

// changement de console

// commandemaster 0 = console de Base
// commandemaster 1 = console de combat
// commandemaster 2 = console de special 
// commandemaster 3 = console d'objet 
// commandemaster 4 = console de defense 



function swapconsole(elem1, elem2) {
    elem1.style.display = 'none';
    elem2.style.display = 'grid';
}


// CONSOLE D'ATTAQUE


choiceAttack.addEventListener('click', function (event) {

    if (endFight(hero, wolf, resultFight) === 0) {
        commandemaster = 1;
        swapconsole(commande, consoleAttack);
    } else if (endFight(hero, bigMonster, resultFight) === 0) {
        commandemaster = 1;
        swapconsole(commande, consoleAttack);
    } else if (endFight(hero, boss, resultFight) === 0) {
        commandemaster = 1;
        swapconsole(commande, consoleAttack);
    }

});


// CONSOLE SPECIAL


choiceSpecial.addEventListener('click', function (event) {

    if (endFight(hero, wolf, resultFight) === 0) {
        commandemaster = 2;
        swapconsole(commande, consoleSpecial);
    } else if (endFight(hero, bigMonster, resultFight) === 0) {
        commandemaster = 2;
        swapconsole(commande, consoleSpecial);
    } else if (endFight(hero, boss, resultFight) === 0) {
        commandemaster = 2;
        swapconsole(commande, consoleSpecial);
    }

});



// CONSOLE OBJET


choiceobject.addEventListener('click', function (event) {

    if (endFight(hero, wolf, resultFight) === 0) {
        commandemaster = 3;
        swapconsole(commande, consoleObject);
    } else if (endFight(hero, bigMonster, resultFight) === 0) {
        commandemaster = 3;
        swapconsole(commande, consoleObject);
    } else if (endFight(hero, boss, resultFight) === 0) {
        commandemaster = 3;
        swapconsole(commande, consoleObject);
    }
});



// CONSOLE DEFENSE


choiceDefense.addEventListener('click', function (event) {
    if (endFight(hero, wolf, resultFight) === 0) {
        commandemaster = 4;
        swapconsole(commande, consoleDefense);
    } else if (endFight(hero, bigMonster, resultFight) === 0) {
        commandemaster = 4;
        swapconsole(commande, consoleDefense);
    } else if (endFight(hero, boss, resultFight) === 0) {
        commandemaster = 4;
        swapconsole(commande, consoleDefense);
    }
});



buttonBack.addEventListener('click', function (event) {
    if (commandemaster === 1) {
        swapconsole(consoleAttack, commande);
    }
    if (commandemaster === 2) {
        swapconsole(consoleSpecial, commande);
    }
    if (commandemaster === 3) {
        swapconsole(consoleObject, commande);
    }
    if (commandemaster === 4) {
        swapconsole(consoleDefense, commande);
    }
    commandemaster = 0;
});