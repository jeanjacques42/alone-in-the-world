// START PARTI 1
var wolfTamed = localStorage.getItem('wolf');
if (wolfTamed === '1') {
    let imgwolf = document.createElement("img");
    let src = document.getElementById("background");
    imgwolf.id = "imageWolftamed";
    imgwolf.src = "image/wolf.webp";
    src.appendChild(imgwolf);
    document.querySelector(".text-adjust p").innerHTML = `prepare to fight wolf!`;
}






// RANDOMIZATION DU MECHANT DE LA FORET 

if (random === 0) {
    document.querySelector('body').appendChild(script);
    script.src = 'js/gobelinFight.js';
    forestOpponent = gobelin;
    let src = document.getElementById("background");
    monster.id = "gobelin";
    monster.src = "image/gobelin.png";
    document.querySelector(`.opponentBar`).classList.add('gobelinBar');
    src.appendChild(monster);
} else if (random === 1) {
    document.querySelector('body').appendChild(script);
    script.src = 'js/trollFight.js';
    forestOpponent = troll;
    let src = document.getElementById("background");
    monster.id = "troll";
    monster.src = "image/troll.png";
    document.querySelector(`.opponentBar`).classList.add('trollBar');
    src.appendChild(monster);
} else if (random === 2) {
    document.querySelector('body').appendChild(script);
    script.src = 'js/necromancerFight.js';
    forestOpponent = necromancer;
    let src = document.getElementById("background");
    monster.id = "necromancer";
    monster.src = "image/necromancer.png";
    document.querySelector(`.opponentBar`).classList.add('necromancerBar');
    src.appendChild(monster);
} else if (random === 3) {
    document.querySelector('body').appendChild(script);
    script.src = 'js/robotFight.js';
    forestOpponent = robot;
    let src = document.getElementById("background");
    monster.id = "robot";
    monster.src = "image/robot.png";
    document.querySelector(`.opponentBar`).classList.add('robotBar');
    src.appendChild(monster);
} else if (random === 4) {
    document.querySelector('body').appendChild(script);
    script.src = 'js/banditFight.js';
    forestOpponent = bandit;
    let src = document.getElementById("background");
    monster.id = "bandit";
    monster.src = "image/bandit.png";
    document.querySelector(`.opponentBar`).classList.add('banditBar');
    src.appendChild(monster);
} else if (random === 5) {
    document.querySelector('body').appendChild(script);
    script.src = 'js/gouleFight.js';
    forestOpponent = goule;
    let src = document.getElementById("background");
    monster.id = "goule";
    monster.src = "image/goule.png";
    document.querySelector(`.opponentBar`).classList.add('gouleBar');
    src.appendChild(monster);
}



// AFFICHAGE DES CARACTERISTIQUES DES MECHANTS

monster.addEventListener('click', function (event) {
    if (verifDialogue === 0) {
        verifDialogue = 1;
        dialoguebox(verifDialogue);
        document.querySelector(".text-adjust p").innerHTML = `${forestOpponent.name}: <br> health: ${forestOpponent.health} <br> havoc: ${forestOpponent.havoc} <br> `;
    }
});

imghero.addEventListener('click', function (event) {
    if (verifDialogue === 0) {
        verifDialogue = 1;
        dialoguebox(verifDialogue);
        document.querySelector(".text-adjust p").innerHTML = `${hero.name}: <br> health:${hero.health} <br> havoc:${hero.havoc} <br> dodge:${hero.dodge} <br> criticalStrike:${hero.criticalStrike}`;
    }
});




// initialisation des objet
document.querySelector(".consoleObject .object1 .console-tag").innerHTML = `${hero.inventory[0]}`;
document.querySelector(".consoleObject .object2 .console-tag").innerHTML = `${hero.inventory[1]}`;
document.querySelector(".consoleObject .object3 .console-tag").innerHTML = `${hero.inventory[2]}`;
document.querySelector(".consoleObject .object4 .console-tag").innerHTML = `${hero.inventory[3]}`;




let next = document.querySelector('#event img');
next.addEventListener('click', function (event) {
    console.log('ok');
    window.location = 'choose.html';
});