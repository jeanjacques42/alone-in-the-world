class Opponent {
    /**
     * 
     * @param {string} name 
     * @param {number} maxHealth 
     * @param {number} health 
     * @param {number} havoc 
     * @param {number} dodge 
     * @param {number} criticalStrike 
     * 
     */

    constructor(name, maxHealth, health, havoc, dodge, criticalStrike) {
        this.name = name;
        this.maxHealth = maxHealth
        this.health = health;
        this.havoc = havoc;
        this.dodge = dodge;
        this.criticalStrike = criticalStrike;
    }
}