// systeme de combat

// CONSOLE DE COMBAT starts here!

// PUNCH 
verifDialogue = 1;
dialoguebox(verifDialogue);
document.querySelector(".text-adjust p").innerHTML = 'what is this ??';


punchcommande.addEventListener('click', function (event) {
    punch(hero, robot);
    swapconsole(consoleAttack, commande);
    setTimeout(function () {
        // imageHero.className = 'fist';
        imageHero.src = 'image/heroFist.png';
        imageHero.style.width = '280px';
        imageHero.style.height = '303px';
    }, 200);
    setTimeout(function () {
        imageHero.src = 'image/hero.png';
        // imageHero.classList.remove('fist');
        imageHero.style.width = '117px';
    }, 700);
    if (endFight(hero, robot, resultFight) === 1) {
        let elem = document.querySelector("#robot");
        elem.remove();
    }
    if (robot.health > 0) {
        opponentTurn(hero, robot);
        setTimeout(function () {
            document.querySelector('#robot').style.marginLeft = '142px';
        }, 1000);
        setTimeout(function () {
            document.querySelector('#robot').style.marginLeft = '222px';
        }, 1500);
        verifDialogue = 1;
        dialoguebox(verifDialogue);
    }
});


function punch(hero, opponent) {
    opponent.health = opponent.health - hero.havoc;
    let opponentBar = document.querySelector(`.opponentBar span`);
    let pourcentageVie = Math.floor((opponent.health / opponent.maxHealth) * 100);
    opponentBar.style.width = `${pourcentageVie}%`;
    if (opponent.health <= 0) {
        opponentBar.style.width = `0%`;
    }
}


// KICK 


kickcommande.addEventListener('click', function (event) {

    kick(hero, robot);
    swapconsole(consoleAttack, commande);
    setTimeout(function () {
        imageHero.src = 'image/heroKick.png';
        imageHero.style.width = '405px';
        imageHero.style.height = '303px';
        imageHero.style.marginLeft = '-22px';
    }, 200);
    setTimeout(function () {
        imageHero.src = 'image/hero.png';
        imageHero.style.width = '117px';
        imageHero.style.marginLeft = '50px';
    }, 700);

    if (endFight(hero, robot, resultFight) === 1) {
        let elem = document.querySelector("#robot");
        elem.remove();
    }
    if (robot.health > 0) {
        opponentTurn(hero, robot);
        setTimeout(function () {
            document.querySelector('#robot').style.marginLeft = '142px';
        }, 1000);
        setTimeout(function () {
            document.querySelector('#robot').style.marginLeft = '222px';
        }, 1500);
        verifDialogue = 1;
        dialoguebox(verifDialogue);

    }
});

function kick(hero, opponent) {
    opponent.health = opponent.health - (hero.havoc - 10);
    opponent.havoc = opponent.havoc - 7;
    let opponentBar = document.querySelector(`.opponentBar span`);
    let pourcentageVie = Math.floor((opponent.health / opponent.maxHealth) * 100);
    opponentBar.style.width = `${pourcentageVie}%`;
    if (opponent.health <= 0) {
        opponentBar.style.width = `0%`;
    }
}



// OVERTHROW


overthrowcommande.addEventListener('click', function (event) {

    overthrow(hero, robot);
    swapconsole(consoleAttack, commande);
    setTimeout(function () {
        imageHero.src = 'image/heroOverthrow.png';
        imageHero.style.width = '204px';
        imageHero.style.height = '303px';
        // imageHero.style.marginLeft = '-22px';
    }, 200);
    setTimeout(function () {
        imageHero.src = 'image/hero.png';
        imageHero.style.width = '117px';
        imageHero.style.marginLeft = '50px';
    }, 700);
    setTimeout(function () {
        document.querySelector('#robot').style.marginLeft = '328px';
    }, 1000);
    setTimeout(function () {
        document.querySelector('#robot').style.marginLeft = '222px';
    }, 1500);
    verifDialogue = 1;
    dialoguebox(verifDialogue);
    document.querySelector(".text-adjust p").innerHTML = `${robot.name}: <br> health:${robot.health} <br> havoc:${robot.havoc} <br> <br> ${hero.name}: <br> health:${hero.health} <br> havoc:${hero.havoc}`;
    if (endFight(hero, robot, resultFight) === 1) {
        let elem = document.querySelector("#robot");
        elem.remove();

    }
});

function overthrow(hero, opponent) {
    opponent.health = opponent.health - 15;
    hero.dodge = hero.dodge + 5;
    let opponentBar = document.querySelector(`.opponentBar span`);
    let pourcentageVie = Math.floor((opponent.health / opponent.maxHealth) * 100);
    opponentBar.style.width = `${pourcentageVie}%`;
    if (opponent.health <= 0) {
        opponentBar.style.width = `0%`;
    }
}


//  WEAPON SHOT


weaponshotcommande.addEventListener('click', function (event) {

    weapon(hero, robot);
    swapconsole(consoleAttack, commande);
    setTimeout(function () {
        imageHero.src = 'image/heroFist.png';
        imageHero.style.width = '280px';
        imageHero.style.height = '303px';
    }, 200);
    setTimeout(function () {
        imageHero.src = 'image/hero.png';
        imageHero.style.width = '117px';
        imageHero.style.marginLeft = '50px';
    }, 300);
    setTimeout(function () {
        imageHero.src = 'image/heroFist.png';
        imageHero.style.width = '280px';
        imageHero.style.height = '303px';
    }, 400);
    setTimeout(function () {
        imageHero.src = 'image/hero.png';
        imageHero.style.width = '117px';
        imageHero.style.marginLeft = '50px';
    }, 500);
    setTimeout(function () {
        imageHero.src = 'image/heroFist.png';
        imageHero.style.width = '280px';
        imageHero.style.height = '303px';
    }, 600);
    setTimeout(function () {
        imageHero.src = 'image/hero.png';
        imageHero.style.width = '117px';
        imageHero.style.marginLeft = '50px';
    }, 700);
    setTimeout(function () {
        imageHero.src = 'image/heroFist.png';
        imageHero.style.width = '280px';
        imageHero.style.height = '303px';
    }, 800);
    setTimeout(function () {
        imageHero.src = 'image/hero.png';
        imageHero.style.width = '117px';
        imageHero.style.marginLeft = '50px';
    }, 900);

    if (endFight(hero, robot, resultFight) === 1) {
        let elem = document.querySelector("#robot");
        elem.remove();
    }
    if (robot.health > 0) {
        opponentTurn(hero, robot);
        setTimeout(function () {
            document.querySelector('#robot').style.marginLeft = '142px';
        }, 1000);
        setTimeout(function () {
            document.querySelector('#robot').style.marginLeft = '222px';
        }, 1500);
        verifDialogue = 1;
        dialoguebox(verifDialogue);

    }
});


function weapon(hero, opponent) {
    opponent.health = opponent.health - (hero.havoc + hero.weapon);
    let opponentBar = document.querySelector(`.opponentBar span`);
    let pourcentageVie = Math.floor((opponent.health / opponent.maxHealth) * 100);
    opponentBar.style.width = `${pourcentageVie}%`;
    if (opponent.health <= 0) {
        opponentBar.style.width = `0%`;
    }
}


// CONSOLE DE COMBAT ends here!


// CONSOLE DE SPECIAL

// SPECIAL


mantracommande.addEventListener('click', function (event) {

    mantra(hero, robot);
    swapconsole(consoleSpecial, commande);
    setTimeout(function () {
        // imageHero.className = 'fist';
        imageHero.src = 'image/heroMantra.png';
        imageHero.style.width = '240px';
        imageHero.style.height = '303px';
    }, 200);
    setTimeout(function () {
        imageHero.src = 'image/hero.png';
        // imageHero.classList.remove('fist');
        imageHero.style.width = '117px';
    }, 900);

    if (endFight(hero, robot, resultFight) === 1) {
        let elem = document.querySelector("#robot");
        elem.remove();
    }
    if (robot.health > 0) {
        opponentTurn(hero, robot);
        setTimeout(function () {
            document.querySelector('#robot').style.marginLeft = '142px';
        }, 1000);
        setTimeout(function () {
            document.querySelector('#robot').style.marginLeft = '222px';
        }, 1500);
        verifDialogue = 1;
        dialoguebox(verifDialogue);

    }
});


function mantra(hero, opponent) {
    hero.health = hero.health + 25;
    hero.havoc = hero.havoc + 15;
    hero.criticalStrike = hero.criticalStrike + 25;
}




// CONSOLE OBJET



objectcommande.addEventListener('click', function (event) {
    if (empty === 0) {
        object1(hero, robot, empty);
        swapconsole(consoleObject, commande);
        hero.inventory[0] = 'empty';
        document.querySelector(".consoleObject .object1 .console-tag").innerHTML = `${hero.inventory[0]}`;
        if (endFight(hero, robot, resultFight) === 1) {
            let elem = document.querySelector("#robot");
            elem.remove();
        }
        if (robot.health > 0 && robot.tame < 2) {
            opponentTurn(hero, robot);
            document.querySelector(".text-adjust p").innerHTML = `you use raw rabbit`;
            verifDialogue = 1;
            dialoguebox(verifDialogue);
            empty = 1;
        }
    } else if (empty === 1) {
        verifDialogue = 1;
        dialoguebox(verifDialogue);
        document.querySelector(".text-adjust p").innerHTML = `it's empty`;
    }

});


function object1(hero, opponent, empty) {
    opponent.tame = opponent.tame + 1;
}






object2commande.addEventListener('click', function (event) {
    if (empty2 === 0) {
        object2(hero, robot, empty);
        swapconsole(consoleObject, commande);
        hero.inventory[1] = 'empty';
        document.querySelector(".consoleObject .object2 .console-tag").innerHTML = `${hero.inventory[1]}`;
        if (endFight(hero, robot, resultFight) === 1) {
            let elem = document.querySelector("#robot");
            elem.remove();
        }
        if (robot.health > 0 && robot.tame < 2) {
            opponentTurn(hero, robot);
            document.querySelector(".text-adjust p").innerHTML = `you use your torch, robot is scared, you can now bring you closer to the robot ..`;
            verifDialogue = 1;
            dialoguebox(verifDialogue);
            empty2 = 1;
        }
    } else if (empty2 === 1) {
        verifDialogue = 1;
        dialoguebox(verifDialogue);
        document.querySelector(".text-adjust p").innerHTML = `it's empty`;
    }

});


function object2(hero, opponent) {
    opponent.tame = opponent.tame + 1;
}




// CONSOLE DE DEFENSE 

// guard


guardcommande.addEventListener('click', function (event) {

    guard(hero, robot);
    swapconsole(consoleDefense, commande);
    setTimeout(function () {
        // imageHero.className = 'fist';
        imageHero.src = 'image/heroGuard.png';
        imageHero.style.width = '161px';
        imageHero.style.height = '333px';
    }, 200);
    setTimeout(function () {
        // imageHero.src = 'image/heroGuard.png';
        // imageHero.style.width = '191px';
        // imageHero.style.height = '343px';

        imageHero.src = 'image/hero.png';
        // imageHero.classList.remove('fist');
        imageHero.style.width = '117px';
        imageHero.style.height = '303px';
    }, 900);
    if (endFight(hero, robot, resultFight) === 1) {
        let elem = document.querySelector("#robot");
        elem.remove();
    }
    if (robot.health > 0 || robot.tame < 2) {
        opponentTurn(hero, robot);
        setTimeout(function () {
            document.querySelector('#robot').style.marginLeft = '142px';
        }, 1000);
        setTimeout(function () {
            document.querySelector('#robot').style.marginLeft = '222px';
        }, 1500);
        verifDialogue = 1;
        dialoguebox(verifDialogue);

    }
});

function guard(hero, opponent, tame) {
    hero.havoc = hero.havoc + 15;
    hero.dodge = hero.dodge + 5;
    hero.position = 'wolf';
    hero.criticalStrike = hero.criticalStrike + 5;
}




// riposte


ripostecommande.addEventListener('click', function (event) {

    riposte(hero, robot);
    swapconsole(consoleDefense, commande);
    setTimeout(function () {
        // imageHero.className = 'fist';
        imageHero.src = 'image/heroRiposte.png';
        imageHero.style.width = '235px';
        imageHero.style.height = '342px';
    }, 200);
    setTimeout(function () {
        imageHero.src = 'image/hero.png';
        // imageHero.classList.remove('fist');
        imageHero.style.width = '117px';
        imageHero.style.height = '303px';

    }, 700);
    if (endFight(hero, robot, resultFight) === 1) {
        let elem = document.querySelector("#robot");
        elem.remove();
    }
    if (robot.health > 0) {
        document.querySelector(".text-adjust p").innerHTML = `${robot.name}: <br> health:${robot.health} <br> havoc:${robot.havoc} <br> <br> ${hero.name}: <br> health:${hero.health} <br> havoc:${hero.havoc}`;
        setTimeout(function () {
            document.querySelector('#robot').style.marginLeft = '328px';
        }, 1000);
        setTimeout(function () {
            document.querySelector('#robot').style.marginLeft = '222px';
        }, 1500);
        verifDialogue = 1;
        dialoguebox(verifDialogue);

    }
});


function riposte(hero, opponent) {
    opponent.health = opponent.health - opponent.havoc;
    hero.dodge = hero.dodge + 5;
    let opponentBar = document.querySelector(`.opponentBar span`);
    let pourcentageVie = Math.floor((opponent.health / opponent.maxHealth) * 100);
    opponentBar.style.width = `${pourcentageVie}%`;
    if (opponent.health <= 0) {
        opponentBar.style.width = `0%`;
    }
}



// crushcounter


crushcountercommande.addEventListener('click', function (event) {

    crushcounter(hero, robot);
    swapconsole(consoleDefense, commande);
    setTimeout(function () {
        document.querySelector('#robot').style.marginLeft = '142px';
    }, 200);
    setTimeout(function () {
        document.querySelector('#robot').style.marginLeft = '222px';
    }, 700);
    setTimeout(function () {
        // imageHero.className = 'fist';
        imageHero.src = 'image/heroCrushCounter.png';
        imageHero.style.width = '235px';
        imageHero.style.height = '342px';
        imageHero.style.marginLeft = '70px';
    }, 1000);
    setTimeout(function () {
        imageHero.src = 'image/hero.png';
        // imageHero.classList.remove('fist');
        imageHero.style.width = '117px';
        imageHero.style.height = '303px';
        imageHero.style.marginLeft = '50px';
    }, 1500);

    if (endFight(hero, robot, resultFight) === 1) {
        let elem = document.querySelector("#robot");
        elem.remove();
    }
    opponentTurn(hero, robot);
    verifDialogue = 1;
    dialoguebox(verifDialogue);


});

function crushcounter(hero, opponent) {
    opponent.health = opponent.health - (40 % hero.maxHealth);
    let opponentBar = document.querySelector(`.opponentBar span`);
    let pourcentageVie = Math.floor((opponent.health / opponent.maxHealth) * 100);
    opponentBar.style.width = `${pourcentageVie}%`;
    if (opponent.health <= 0) {
        opponentBar.style.width = `0%`;
    }
}



// leave


leavecommande.addEventListener('click', function (event) {

    leave(hero, robot);
    swapconsole(consoleDefense, commande);
    document.querySelector(".text-adjust p").innerHTML = 'you leave';
    verifDialogue = 1;
    dialoguebox(verifDialogue);
    fuite = 1;

});

function leave(hero, opponent) {
    hero.dodge = hero.dodge + 5;
}





// EVENT DE FIGHT 
function opponentTurn(hero, opponent) {

    hero.health = hero.health - opponent.havoc;
    document.querySelector(".text-adjust p").innerHTML = `${robot.name}: <br> health: ${robot.health} <br> havoc: ${robot.havoc} <br> fear: everybody `;
    // document.querySelector(".text-adjust p").innerHTML = `${hero.name}: <br> health:${hero.health} <br> havoc:${hero.havoc}`;
    // verifDialogue = 1;
    // dialoguebox(verifDialogue);
    let heroBar = document.querySelector(`.heroBar span`);
    let pourcentageVie = Math.floor((hero.health / hero.maxHealth) * 100);
    if (pourcentageVie > 100) {
        heroBar.style.backgroundColor = 'rgb(218, 129, 14)';
        heroBar.style.width = `110%`;
    } else {
        heroBar.style.backgroundColor = 'rgb(187, 21, 21)';
        heroBar.style.width = `${pourcentageVie}%`;
    }
    if (hero.health <= 0) {
        heroBar.style.width = `0%`;
        verifDialogue = 1;
        dialoguebox(verifDialogue);
        document.querySelector(".text-adjust p").innerHTML = `you are in bad shape you start to lose consciousness little by little .. `;
    }
}


function endFight(hero, opponent, result) {
    if (opponent.health <= 0) {
        result = 1;
        verifDialogue = 1;
        dialoguebox(verifDialogue);
        document.querySelector(".text-adjust p").innerHTML = `${opponent.name} is dead`;

    }
    if (hero.health <= 0) {
        result = 1;
        verifDialogue = 1;
        dialoguebox(verifDialogue);
        document.querySelector(".text-adjust p").innerHTML = `you are in bad shape you start to lose consciousness little by little .. `;
        loose = 1;

    }
    return (result);
}